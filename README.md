<!-- DOCSIBLE START -->

# 📃 Role overview

## silver-forest



Description: your role description


| Field                | Value           |
|--------------------- |-----------------|
| Functional description | Not available. |
| Requester            | Not available. |
| Users                | Not available. |
| Date dev             | Not available. |
| Date prod            | Not available. |
| Readme update            | 23/04/2024 |
| Version              | Not available. |
| Time Saving              | Not available. |
| Category              | Not available. |
| Sub category              | Not available. |
| Critical ⚠️            | Not available. |

### Defaults

**These are static variables with lower priority**

#### File: main.yml



| Var          | Type         | Value       |Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| [min_water_quality_index](defaults/main.yml#L6)   | int   | `50`  |  True  |  Minimum water quality index acceptable |
| [motion_sensitivity](defaults/main.yml#L10)   | int   | `5`  |  True  |  Motion detection sensitivity scale (1-10) |
| [windows_status_expected](defaults/main.yml#L14)   | str   | `closed`  |  True  |  Windows status for security check (open/closed) |
| [doors_status_expected](defaults/main.yml#L18)   | str   | `closed`  |  True  |  Doors status for security check (open/closed) |





### Tasks


#### File: main.yml

| Name | Module | Has Conditions |
| ---- | ------ | --------- |
| Initialize environmental monitoring | ansible.builtin.debug | False |
| Check water quality index | block | False |
| Fetch water quality data | ansible.builtin.set_fact | False |
| Evaluate water quality | ansible.builtin.debug | True |
| Alert on poor water quality | ansible.builtin.debug | True |
| Monitor forest security - doors and windows | block | False |
| Check status of windows | ansible.builtin.debug | False |
| Set current windows status | ansible.builtin.set_fact | False |
| Check status of doors | ansible.builtin.debug | False |
| Set current doors status | ansible.builtin.set_fact | False |
| Alert if windows are not as expected | ansible.builtin.debug | True |
| Alert if doors are not as expected | ansible.builtin.debug | True |
| Motion detection in forest area | block | False |
| Simulate motion detection check | ansible.builtin.debug | False |
| Set fact for motion detection | ansible.builtin.set_fact | False |
| Alert on detected motion based on sensitivity settings | ansible.builtin.debug | True |
| Environmental monitoring completed | ansible.builtin.debug | False |


## Task Flow Graphs



### Graph for main.yml

```mermaid
flowchart TD
Start
classDef block stroke:#3498db,stroke-width:2px;
classDef task stroke:#4b76bb,stroke-width:2px;
classDef include stroke:#2ecc71,stroke-width:2px;
classDef import stroke:#f39c12,stroke-width:2px;
classDef rescue stroke:#665352,stroke-width:2px;
classDef importPlaybook stroke:#9b59b6,stroke-width:2px;
classDef importTasks stroke:#34495e,stroke-width:2px;
classDef includeTasks stroke:#16a085,stroke-width:2px;
classDef importRole stroke:#699ba7,stroke-width:2px;
classDef includeRole stroke:#2980b9,stroke-width:2px;
classDef includeVars stroke:#8e44ad,stroke-width:2px;

  Start-->|Task| Initialize_environmental_monitoring0[initialize environmental monitoring]:::task
  Initialize_environmental_monitoring0-->|Block Start| Check_water_quality_index1_block_start_0[[check water quality index]]:::block
  Check_water_quality_index1_block_start_0-->|Task| Fetch_water_quality_data0[fetch water quality data]:::task
  Fetch_water_quality_data0-->|Task| Evaluate_water_quality1_when_water_quality_index___int____min_water_quality_index[evaluate water quality]:::task
  Evaluate_water_quality1_when_water_quality_index___int____min_water_quality_index---|When: water quality index   int    min water quality<br>index| Evaluate_water_quality1_when_water_quality_index___int____min_water_quality_index
  Evaluate_water_quality1_when_water_quality_index___int____min_water_quality_index-->|Task| Alert_on_poor_water_quality2_when_water_quality_index___int___min_water_quality_index[alert on poor water quality]:::task
  Alert_on_poor_water_quality2_when_water_quality_index___int___min_water_quality_index---|When: water quality index   int   min water quality<br>index| Alert_on_poor_water_quality2_when_water_quality_index___int___min_water_quality_index
  Alert_on_poor_water_quality2_when_water_quality_index___int___min_water_quality_index-.->|End of Block| Check_water_quality_index1_block_start_0
  Alert_on_poor_water_quality2_when_water_quality_index___int___min_water_quality_index-->|Rescue Start| Check_water_quality_index1_rescue_start_0[check water quality index]:::rescue
  Check_water_quality_index1_rescue_start_0-->|Task| Handle_failure_in_water_quality_monitoring0[handle failure in water quality monitoring]:::task
  Handle_failure_in_water_quality_monitoring0-.->|End of Rescue Block| Check_water_quality_index1_block_start_0
  Handle_failure_in_water_quality_monitoring0-->|Block Start| Monitor_forest_security___doors_and_windows2_block_start_0[[monitor forest security   doors and windows]]:::block
  Monitor_forest_security___doors_and_windows2_block_start_0-->|Task| Check_status_of_windows0[check status of windows]:::task
  Check_status_of_windows0-->|Task| Set_current_windows_status1[set current windows status]:::task
  Set_current_windows_status1-->|Task| Check_status_of_doors2[check status of doors]:::task
  Check_status_of_doors2-->|Task| Set_current_doors_status3[set current doors status]:::task
  Set_current_doors_status3-->|Task| Alert_if_windows_are_not_as_expected4_when_windows_status____windows_status_expected[alert if windows are not as expected]:::task
  Alert_if_windows_are_not_as_expected4_when_windows_status____windows_status_expected---|When: windows status    windows status expected| Alert_if_windows_are_not_as_expected4_when_windows_status____windows_status_expected
  Alert_if_windows_are_not_as_expected4_when_windows_status____windows_status_expected-->|Task| Alert_if_doors_are_not_as_expected5_when_doors_status____doors_status_expected[alert if doors are not as expected]:::task
  Alert_if_doors_are_not_as_expected5_when_doors_status____doors_status_expected---|When: doors status    doors status expected| Alert_if_doors_are_not_as_expected5_when_doors_status____doors_status_expected
  Alert_if_doors_are_not_as_expected5_when_doors_status____doors_status_expected-.->|End of Block| Monitor_forest_security___doors_and_windows2_block_start_0
  Alert_if_doors_are_not_as_expected5_when_doors_status____doors_status_expected-->|Rescue Start| Monitor_forest_security___doors_and_windows2_rescue_start_0[monitor forest security   doors and windows]:::rescue
  Monitor_forest_security___doors_and_windows2_rescue_start_0-->|Task| Handle_failure_in_security_status_check0[handle failure in security status check]:::task
  Handle_failure_in_security_status_check0-.->|End of Rescue Block| Monitor_forest_security___doors_and_windows2_block_start_0
  Handle_failure_in_security_status_check0-->|Block Start| Motion_detection_in_forest_area3_block_start_0[[motion detection in forest area]]:::block
  Motion_detection_in_forest_area3_block_start_0-->|Task| Simulate_motion_detection_check0[simulate motion detection check]:::task
  Simulate_motion_detection_check0-->|Task| Set_fact_for_motion_detection1[set fact for motion detection]:::task
  Set_fact_for_motion_detection1-->|Task| Alert_on_detected_motion_based_on_sensitivity_settings2_when_motion_detected_and__motion_sensitivity___int___3_[alert on detected motion based on sensitivity<br>settings]:::task
  Alert_on_detected_motion_based_on_sensitivity_settings2_when_motion_detected_and__motion_sensitivity___int___3_---|When: motion detected and  motion sensitivity   int   3 | Alert_on_detected_motion_based_on_sensitivity_settings2_when_motion_detected_and__motion_sensitivity___int___3_
  Alert_on_detected_motion_based_on_sensitivity_settings2_when_motion_detected_and__motion_sensitivity___int___3_-.->|End of Block| Motion_detection_in_forest_area3_block_start_0
  Alert_on_detected_motion_based_on_sensitivity_settings2_when_motion_detected_and__motion_sensitivity___int___3_-->|Rescue Start| Motion_detection_in_forest_area3_rescue_start_0[motion detection in forest area]:::rescue
  Motion_detection_in_forest_area3_rescue_start_0-->|Task| Handle_motion_detection_system_failure0[handle motion detection system failure]:::task
  Handle_motion_detection_system_failure0-.->|End of Rescue Block| Motion_detection_in_forest_area3_block_start_0
  Handle_motion_detection_system_failure0-->|Task| Environmental_monitoring_completed4[environmental monitoring completed]:::task
  Environmental_monitoring_completed4-->End
```


## Playbook

```yml
---
- hosts: localhost
  connection: local
  roles:
    - role: ../silver-forest

```
## Playbook graph
```mermaid
flowchart TD
  localhost-->|Role| ___silver_forest[   silver forest]
```

## Author Information
Lucian BLETAN

#### License

license (GPL-2.0-or-later, MIT, etc)

#### Minimum Ansible Version

2.1

#### Platforms

No platforms specified.
<!-- DOCSIBLE END -->